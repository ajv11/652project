#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int main(){
  int cacheBlockSize  = pow(2,10); //In bytes
  int cacheBlocksInArr=pow(2,0); 
  int numIntsPerBlock=cacheBlockSize/sizeof(int);
  int arrSize=numIntsPerBlock * cacheBlocksInArr;  
  int* arr=malloc(arrSize*sizeof(int));
  for(int i=0;i<arrSize;i++){
    arr[i]=1;
  }
  
  long sum=0; 
  //Dependent associative operations
  for(int i =0;i<arrSize;i++){
    sum+=arr[i]; 
  }
  
  //Dependent associative operations
  for(int i =0;i<arrSize;i++){
    if(i%2==0){
      sum+=arr[i]; 
    } 
    else{
      sum*=arr[i]; 
    }
  }

  //Independent accesses
  for(int i=0;i<arrSize;i++){
    arr[i]+=arr[i]; 
  }
  
  return EXIT_SUCCESS; 
}
