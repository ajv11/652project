TARGETS=simpleRedux

simpleRedux: simpleRedux.o
	gcc -Wall -Werror simpleRedux.o -o simpleRedux

simpleRedux.o: simpleRedux.c
	gcc -g -c -std=c99 simpleRedux.c

clean: 
	rm *.o $(TARGETS) *~
